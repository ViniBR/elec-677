import numpy as np

def actFun(self, z, type):
    '''
    actFun computes the activation functions
    :param z: net input
    :param type: Tanh, Sigmoid, or ReLU
    :return: activations
    '''

    # YOU IMPLMENT YOUR actFun HERE
    if type == 'Tanh':
        act = np.tanh(z)
    elif type == 'Sigmoid':
        act = 1 / (1 + np.exp(-z))
    elif type == 'ReLU':
        act = max(0, z)

    return act

print(actFun(0, [10, 5, 2, 1, 0, -1, -2, -5, -10], 'ReLU'))
